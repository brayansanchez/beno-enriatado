export interface Oferta{
  id:number;
  restautanteId:string;
  titulo:string;
  subtitulo:string;
  imgUrl: string;
  vigencia:string;
}
