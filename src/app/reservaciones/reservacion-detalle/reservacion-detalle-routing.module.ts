import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReservacionesPage } from '../reservaciones.page';

const routes: Routes = [
 {
 path: ''
,
 component: ReservacionesPage
 },
 {
 path: ':reservacionId',//SOLO MODIFICAMOS ESTA LINEA
 loadChildren: () => import('./reservacion-detalle.module').then( m => m.ReservacionDetallePageModule)
 }
];
@NgModule({
 imports: [RouterModule.forChild(routes)],
 exports: [RouterModule],
})
export class ReservacionesPageRoutingModule {}
